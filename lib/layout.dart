import 'package:flutter/material.dart';
import 'package:responsive_app/helpers/responsiveness.dart';
import 'package:responsive_app/widget/large_screen.dart';
import 'package:responsive_app/widget/side_menu.dart';
import 'package:responsive_app/widget/small_screen.dart';
import 'package:responsive_app/widget/top_nav.dart';

class SiteLayout extends StatelessWidget {
  SiteLayout({Key? key}) : super(key: key);

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      extendBodyBehindAppBar: true,
      appBar: topNavigationBar(context, scaffoldKey),
      drawer: const Drawer(
        child: SideMenu(),
      ),
      body: const ResponsiveWidget(
        largeScreen: LargeScreen(),
        smallScreen: SmallScreen(),
      ),
    );
  }
}
