import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:responsive_app/layout.dart';
import 'package:responsive_app/pages/authentication/authentication_page.dart';
import 'package:responsive_app/routing/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      initialRoute: authenticationPageRoute,
      routes: {
        rootRoute:(context)=> SiteLayout(),
        authenticationPageRoute:(context)=>AuthenticationPage(),
        // overviewPageRoute:(context)=>OverviewPage(),
        // driversPageRoute:(context)=>DriverPage(),
        // clientsPageRoute:(context)=>ClientPage(),
      },
      title: "Dash",
      theme: ThemeData(
        scaffoldBackgroundColor: Colors.white,
        textTheme:
            GoogleFonts.mulishTextTheme(Theme.of(context).textTheme).apply(
          bodyColor: Colors.black,
        ),
        pageTransitionsTheme: const PageTransitionsTheme(builders: {
          TargetPlatform.iOS: FadeUpwardsPageTransitionsBuilder(),
          TargetPlatform.android: FadeUpwardsPageTransitionsBuilder(),
        }),
        primaryColor: Colors.blue,
      ),
      // home: AuthenticationPage(),
    );
  }
}
