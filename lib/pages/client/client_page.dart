import 'package:flutter/material.dart';
import 'package:responsive_app/helpers/responsiveness.dart';
import 'package:responsive_app/pages/client/clients_table.dart';
import 'package:responsive_app/widget/custom_text.dart';

class ClientPage extends StatelessWidget {
  const ClientPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: ResponsiveWidget.isSmallScreen(context) ? 56 : 6,
              ),
              child: CustomText(
                text: "Clients",
                size: 24,
                weight: FontWeight.bold,
              ),
            ),
          ],
        ),
        Expanded(
          child: ListView(
            children: [
              ClientsTable(),
            ],
          ),
        )
      ],
    );
  }
}