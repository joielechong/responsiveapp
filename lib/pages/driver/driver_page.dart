import 'package:flutter/material.dart';
import 'package:responsive_app/helpers/responsiveness.dart';
import 'package:responsive_app/pages/driver/drivers_table.dart';
import 'package:responsive_app/widget/custom_text.dart';

class DriverPage extends StatelessWidget {
  const DriverPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: ResponsiveWidget.isSmallScreen(context) ? 56 : 6,
              ),
              child: CustomText(
                text: "Drivers",
                size: 24,
                weight: FontWeight.bold,
              ),
            ),
          ],
        ),
        Expanded(
          child: ListView(
            children: [
              DriversTable(),
            ],
          ),
        )
      ],
    );
  }
}
