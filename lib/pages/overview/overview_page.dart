import 'package:flutter/material.dart';
import 'package:responsive_app/helpers/responsiveness.dart';
import 'package:responsive_app/pages/overview/widgets/available_drivers.dart';
import 'package:responsive_app/pages/overview/widgets/overview_cards_large.dart';
import 'package:responsive_app/pages/overview/widgets/overview_cards_medium.dart';
import 'package:responsive_app/pages/overview/widgets/overview_cards_small.dart';
import 'package:responsive_app/widget/custom_text.dart';
import 'package:responsive_app/widget/revenoue_section_large.dart';
import 'package:responsive_app/widget/revenue_section_small.dart';

class OverviewPage extends StatelessWidget {
  const OverviewPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: ResponsiveWidget.isSmallScreen(context) ? 56 : 6,
              ),
              child: CustomText(
                text: "Overview",
                size: 24,
                weight: FontWeight.bold,
              ),
            ),
          ],
        ),
        Expanded(
          child: ListView(
            children: [
              if (ResponsiveWidget.isLargeScreen(context) ||
                  ResponsiveWidget.isMediumScreen(context))
                if (ResponsiveWidget.isCustomSize(context))
                  OverviewCardsMedium()
                else
                  OverviewCardsLarge()
              else
                OverviewCardsSmall(),
              if (ResponsiveWidget.isSmallScreen(context))
                RevenueSectionSmall()
              else
                RevenueSectionLarge(),

              AvailableDrivers(),
            ],
          ),
        )
      ],
    );
  }
}
