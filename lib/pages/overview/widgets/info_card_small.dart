import 'package:flutter/material.dart';
import 'package:responsive_app/const/style.dart';
import 'package:responsive_app/widget/custom_text.dart';

class InfoCardSmall extends StatelessWidget {
  final String title;
  final String value;
  final Color? topColor;
  final bool? isActive;
  final VoidCallback onTap;

  const InfoCardSmall({
    Key? key,
    required this.title,
    required this.value,
    this.topColor,
    this.isActive=false,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(24),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(
              color: isActive??false ? active : lightGrey,
              width: .5,
            ),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              CustomText(
                text: title,
                size: 24,
                weight: FontWeight.w300,
                color: isActive??false ? active : lightGrey,
              ),
              CustomText(
                text: value,
                size: 24,
                weight: FontWeight.bold,
                color: isActive??false ? active : lightGrey,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
