import 'package:flutter/material.dart';

import 'info_card.dart';

class OverviewCardsMedium extends StatelessWidget {
  const OverviewCardsMedium({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Row(
          children: [
            InfoCard(
              title: "Rides in progress",
              value: '7',
              onTap: () {},
              topColor: Colors.orange,
            ),
            SizedBox(width: width / 64),
            InfoCard(
              title: "Packages delivered",
              value: '17',
              onTap: () {},
              topColor: Colors.lightGreen,
            ),

          ],
        ),
        SizedBox(height: 16),
        Row(
          children: [
            InfoCard(
              title: "Cancelled delivery",
              value: '3',
              onTap: () {},
              topColor: Colors.redAccent,
            ),
            SizedBox(width: width / 64),
            InfoCard(
              title: "Scheduled deliveries",
              value: '3',
              onTap: () {},
            ),
          ],
        )
      ],
    );
  }
}
