import 'package:flutter/material.dart';
import 'package:responsive_app/pages/overview/widgets/info_card_small.dart';

class OverviewCardsSmall extends StatelessWidget {
  const OverviewCardsSmall({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var width = MediaQuery.of(context).size.width;

    return Container(
      height: 400,
      child: Column(
        children: [
          InfoCardSmall(
            title: "Rides in progress",
            value: '7',
            onTap: () {},
            topColor: Colors.orange,
            isActive: true,
          ),
          SizedBox(height: width/64),
          InfoCardSmall(
            title: "Packages delivered",
            value: '17',
            onTap: () {},
            topColor: Colors.lightGreen,
          ),
          SizedBox(height: width/64),
          InfoCardSmall(
            title: "Cancelled delivery",
            value: '3',
            onTap: () {},
            topColor: Colors.redAccent,
          ),
          SizedBox(width: width / 64),
          InfoCardSmall(
            title: "Scheduled deliveries",
            value: '3',
            onTap: () {},
          ),
        ],
      ),
    );
  }
}
