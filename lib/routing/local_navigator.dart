import 'package:flutter/material.dart';
import 'package:responsive_app/routing/navigation_controller.dart';
import 'package:responsive_app/routing/router.dart';
import 'package:responsive_app/routing/routes.dart';

Navigator localNavigator(NavigationController controller) {
  return Navigator(
    key: controller.navigationKey,
    initialRoute: overviewPageRoute,
    onGenerateRoute: generateRoute,
  );
}
