
import 'package:flutter/material.dart';
import 'package:responsive_app/pages/authentication/authentication_page.dart';
import 'package:responsive_app/routing/routes.dart';

class NavigationController {
  static final instance = NavigationController._privateConstructor();
  NavigationController._privateConstructor();
  final GlobalKey<NavigatorState> navigationKey = GlobalKey();

  Future<dynamic> navigateTo(BuildContext c, String routeName) {
   // return Navigator.pushNamed(c, routeName);
    return navigationKey.currentState!.pushNamed(routeName);
  }

  void pushAndRemoveUntil(BuildContext context, Widget page) {
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (BuildContext context) => page),
        ModalRoute.withName('/')
    );
  }
}