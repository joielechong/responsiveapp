import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:responsive_app/layout.dart';
import 'package:responsive_app/pages/authentication/authentication_page.dart';
import 'package:responsive_app/pages/client/client_page.dart';
import 'package:responsive_app/pages/driver/driver_page.dart';
import 'package:responsive_app/pages/overview/overview_page.dart';
import 'package:responsive_app/routing/routes.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case rootRoute:
      return _getPageRoute(SiteLayout());
    case overviewPageRoute:
      return _getPageRoute(OverviewPage());
    case driversPageRoute:
      return _getPageRoute(DriverPage());
    case clientsPageRoute:
      return _getPageRoute(ClientPage());
    case authenticationPageRoute:
      return _getPageRoute(AuthenticationPage());
    default:
      return _getPageRoute(OverviewPage());
  }
}

PageRoute _getPageRoute(Widget child) {
  return MaterialPageRoute(builder: (ctx) => child);
}
