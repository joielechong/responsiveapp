import 'package:flutter/material.dart';

class MenuItem {
  final String name;
  final String route;
  final IconData icon;

  MenuItem(this.name, this.route, this.icon);
}

const rootRoute = "/";
const overviewPageRoute = "/overview";
const driversPageRoute = "/drivers";
const clientsPageRoute = "/clients";
const authenticationPageRoute = "/auth";

List<MenuItem> sideMenuItemsRoutes(BuildContext context) {
  return [
    MenuItem("OverView", overviewPageRoute, Icons.trending_up),
    MenuItem("Drivers", driversPageRoute, Icons.drive_eta),
    MenuItem("Clients", clientsPageRoute, Icons.people_alt_outlined),
    MenuItem("Log Out", authenticationPageRoute, Icons.exit_to_app),
  ];
}
