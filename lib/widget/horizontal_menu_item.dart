import 'package:flutter/material.dart';
import 'package:responsive_app/const/style.dart';

import 'custom_text.dart';

class HorizontalMenuItem extends StatefulWidget {
  final IconData icon;
  final bool isActive;
  final String name;
  final VoidCallback onTap;

  const HorizontalMenuItem(this.icon, this.isActive, this.name,
      {required this.onTap, Key? key})
      : super(key: key);

  @override
  State<HorizontalMenuItem> createState() => _HorizontalMenuItemState();
}

class _HorizontalMenuItemState extends State<HorizontalMenuItem> {
  bool _isHovered = false;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return InkWell(
      onTap: widget.onTap,
      onHover: (value) {
        setState(() => _isHovered = value);
      },
      child: Container(
        color: _isHovered ? lightGrey.withOpacity(0.1) : Colors.transparent,
        child: Row(
          children: [
            Visibility(
              visible: _isHovered,
              maintainSize: true,
              maintainState: true,
              maintainAnimation: true,
              child: Container(
                width: 6,
                height: 40,
                color: dark,
              ),
            ),
            SizedBox(width: width / 88),
            Padding(
              padding: const EdgeInsets.all(16),
              child: widget.isActive
                  ? Icon(
                      widget.icon,
                      size: 22,
                      color: dark,
                    )
                  : Icon(
                      widget.icon,
                      color: _isHovered ? dark : lightGrey,
                    ),
            ),
            Flexible(
              child: widget.isActive
                  ? CustomText(
                      text: widget.name,
                      color: dark,
                      size: 18,
                      weight: FontWeight.bold,
                    )
                  : CustomText(
                      text: widget.name,
                      color: _isHovered ? dark : lightGrey,
                    ),
            )
          ],
        ),
      ),
    );
  }
}
