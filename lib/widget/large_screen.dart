import 'package:flutter/material.dart';
import 'package:responsive_app/routing/local_navigator.dart';
import 'package:responsive_app/routing/navigation_controller.dart';
import 'package:responsive_app/widget/side_menu.dart';

class LargeScreen extends StatelessWidget {
  const LargeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        const Expanded(child: SideMenu()),
        Expanded(
          flex: 5,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: localNavigator(NavigationController.instance),
          ),
        )
      ],
    );
  }
}
