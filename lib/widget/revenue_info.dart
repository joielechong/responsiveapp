import 'package:flutter/material.dart';
import 'package:responsive_app/const/style.dart';

class RevenueInfo extends StatelessWidget {
  final String title;
  final String amount;

  const RevenueInfo(this.title, this.amount, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          children: [
            TextSpan(
                text: "$title \n\n",
                style: TextStyle(color: lightGrey, fontSize: 16)),
            TextSpan(
                text: "\$ $amount ",
                style: TextStyle(fontSize: 24,
                    color: dark,
                    fontWeight: FontWeight.bold))
          ],
        ),
      ),
    );
  }
}
