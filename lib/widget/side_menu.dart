import 'package:flutter/material.dart';
import 'package:responsive_app/const/style.dart';
import 'package:responsive_app/helpers/responsiveness.dart';
import 'package:responsive_app/routing/navigation_controller.dart';
import 'package:responsive_app/routing/routes.dart';
import 'package:responsive_app/widget/custom_text.dart';
import 'package:responsive_app/widget/side_menu_controller.dart';
import 'package:responsive_app/widget/side_menu_item_widget.dart';

class SideMenu extends StatefulWidget {
  const SideMenu({Key? key}) : super(key: key);

  @override
  State<SideMenu> createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  int _selectedIndex = -1;

  final controller = SideMenuController(selectedMenuIndex: 0);

  @override
  void initState() {
    _selectedIndex = controller.selectedMenuIndex;

    controller.addListener(() {
      setState(() {
        _selectedIndex = controller.selectedMenuIndex;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Container(
      color: light,
      child: ListView(
        children: [
          if (ResponsiveWidget.isSmallScreen(context))
            Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                const SizedBox(height: 40),
                Row(
                  children: [
                    SizedBox(width: width / 48),
                    Padding(
                      padding: const EdgeInsets.only(right: 12),
                      child: Image.asset(
                        "assets/icons/rilixtech.png",
                        width: 100,
                        height: 100,
                      ),
                    ),
                    Flexible(
                      child: CustomText(
                        text: "Rilixtech",
                        size: 20,
                        weight: FontWeight.bold,
                        color: active,
                      ),
                    ),
                    SizedBox(width: width / 48)
                  ],
                ),
                SizedBox(height: 40),
              ],
            ),
          if (ResponsiveWidget.isSmallScreen(context))
            Divider(color: lightGrey.withOpacity(.1)),
          Column(
            mainAxisSize: MainAxisSize.min,
            children: sideMenuItemsRoutes(context).asMap().entries.map(
              (mapItem) {
                var item = mapItem.value;
                var idx = mapItem.key;
                return SideMenuItemWidget(
                  item.name,
                  item.icon,
                  idx == _selectedIndex,
                  onTap: () {
                    if (item.route == authenticationPageRoute) {
                      Navigator.pushReplacementNamed(
                          context, authenticationPageRoute);
                      return;
                    }

                    if (idx != _selectedIndex) {
                      if (ResponsiveWidget.isSmallScreen(context)) {
                        Navigator.pop(context);
                      }
                      NavigationController.instance
                          .navigateTo(context, item.route);
                    }

                    controller.setSelectedIndex(idx);
                  },
                );
              },
            )
                // .map((item) => SideMenuItem(item.name, item.icon, false))
                .toList(),
          )
        ],
      ),
    );
  }
}
