import 'package:flutter/material.dart';

class SideMenuController extends ChangeNotifier {
  late int _selectedMenuIndex;

  SideMenuController({selectedMenuIndex = 0}) {
    _selectedMenuIndex = selectedMenuIndex;
  }

  void setSelectedIndex(int index) {
    _selectedMenuIndex = index;
    notifyListeners();
  }

  int get selectedMenuIndex => _selectedMenuIndex;
}
