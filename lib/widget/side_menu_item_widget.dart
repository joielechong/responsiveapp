import 'package:flutter/material.dart';
import 'package:responsive_app/helpers/responsiveness.dart';
import 'package:responsive_app/widget/horizontal_menu_item.dart';
import 'package:responsive_app/widget/vertical_menu_item.dart';

class SideMenuItemWidget extends StatelessWidget {
  final String name;
  final IconData icon;
  final bool isActive;
  final VoidCallback onTap;

  const SideMenuItemWidget(this.name, this.icon, this.isActive,
      {required this.onTap, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (ResponsiveWidget.isCustomSize(context)) {
      return VerticalMenuItem(icon, isActive, name, onTap: onTap);
    }

    return HorizontalMenuItem(icon, isActive, name, onTap: onTap);
  }
}
