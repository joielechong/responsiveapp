import 'package:flutter/material.dart';
import 'package:responsive_app/routing/local_navigator.dart';
import 'package:responsive_app/routing/navigation_controller.dart';

class SmallScreen extends StatelessWidget {
  const SmallScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: localNavigator(NavigationController.instance),
    );
  }
}
