import 'package:flutter/material.dart';
import 'package:responsive_app/const/style.dart';

import 'custom_text.dart';

class VerticalMenuItem extends StatefulWidget {
  final IconData icon;
  final bool isActive;
  final String name;
  final VoidCallback onTap;

  const VerticalMenuItem(this.icon, this.isActive, this.name,
      {required this.onTap, Key? key})
      : super(key: key);

  @override
  State<VerticalMenuItem> createState() => _VerticalMenuItemState();
}

class _VerticalMenuItemState extends State<VerticalMenuItem> {
  bool _isHovered = false;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.onTap,
      onHover: (value) {
        debugPrint("Is hovered $value");
        setState(() => _isHovered = value);
      },
      child: Container(
        color: _isHovered ? lightGrey.withOpacity(0.1) : Colors.transparent,
        child: Row(
          children: [
            Visibility(
              visible: _isHovered,
              maintainSize: true,
              maintainState: true,
              maintainAnimation: true,
              child: Container(
                width: 3,
                height: 72,
                color: Colors.white,
              ),
            ),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(16),
                    child: widget.isActive
                        ? Icon(
                            widget.icon,
                            size: 22,
                            color: dark,
                          )
                        : Icon(
                            widget.icon,
                            color: _isHovered ? dark : lightGrey,
                          ),
                  ),
                  Flexible(
                    child: widget.isActive
                        ? CustomText(
                            text: widget.name,
                            color: dark,
                            size: 18,
                            weight: FontWeight.bold,
                          )
                        : CustomText(
                            text: widget.name,
                            color: _isHovered ? dark : lightGrey,
                          ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
